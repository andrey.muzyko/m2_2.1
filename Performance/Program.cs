﻿using System;
using System.Diagnostics;
using Classes;

namespace Performance
{
    class Program
    {
        static long MyMemorySize(long Memory, string str)
        {
            Memory = Process.GetCurrentProcess().PrivateMemorySize64 - Memory;
            Console.WriteLine($" PrivateMemorySize64 for {str} Array: {Memory / 1024} Kb");
            return Memory;
        }


        static void Main(string[] args)
        {
            const int ElementCount = 100000;
            const int MaxNumber = 99999999;
            Random rnd = new();

            var cMemory = Process.GetCurrentProcess().PrivateMemorySize64;
            var classes = new C[ElementCount];            
            for (int j = 0; j < ElementCount; j++)
            {
                classes[j] = new C(rnd.Next(0, MaxNumber));
            }
            cMemory = MyMemorySize(cMemory, "Class");

            var sMemory = Process.GetCurrentProcess().PrivateMemorySize64;
            var structs = new S[ElementCount];
            for (int j = 0; j < ElementCount; j++)
            {
                structs[j] = new S(rnd.Next(0, MaxNumber));
            }
            sMemory = MyMemorySize(sMemory, "Struct");

            Console.WriteLine($" PrivateMemorySize64 difference for Class and Struct Arrays: {(cMemory - sMemory) / 1024} Kb");
            Console.WriteLine("");

            Stopwatch sw = Stopwatch.StartNew();
            Array.Sort(classes);
            sw.Stop();
            Console.WriteLine(" Class Array Sort Time: {0}ms", sw.Elapsed.TotalMilliseconds);

            sw = Stopwatch.StartNew();
            Array.Sort(structs);
            sw.Stop();
            Console.WriteLine(" Struct Array Sort Time: {0}ms", sw.Elapsed.TotalMilliseconds);

            Console.ReadLine();
        }
    }
}
