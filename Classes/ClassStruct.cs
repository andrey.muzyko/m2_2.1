﻿using System;

namespace Classes
{
    public class C : IComparable<C>
    {
        private int i;

    public C(int i)
        {
            this.i = i;
        }
        public int CompareTo(C c)
        {
            return this.i.CompareTo(c.i);
        }
    }

    public struct S : IComparable<S>
    {
        private int i;


        public S(int i)
        {
            this.i = i;
        }
        public int CompareTo(S s)
        {
            return this.i.CompareTo(s.i);
        }
    }

}
